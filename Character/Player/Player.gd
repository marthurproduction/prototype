extends "res://Character/Character.gd"

var isCasting

func _ready():
	SpellBook.load_spells()
	isCasting = false
	toggle 		= false

# \fn Spell search_spell(spellName)
# \brief Search spellName in the SpellBook
# \param spellName: String. The key to access the spell in the SpellBook.
# \return Spell. New instance Spell with the chosen name. 
# \return Randomly generated spell if not in the SpellBook 
# TODO returns nix for now
func search_spell(spellName):
	return SpellBook.get_spell(spellName)

# \fn void start_cast()
# \brief Activate when player starts to cast a spell
func start_cast():
	chant = "" # clears the player's previous input
	isCasting = true


# TODO seperate "check if chant valid" to another function

# \fn void cast_spell(spell)
# \brief Checks if the spell is complete. Cast the spell if chants are the same
# \details Should be called on the "_process" to work.
# \details Needs to check regularly if the spell is complete.
# \param spell: Spell. The spell casted.
func cast_spell(spell):
	if isCasting and (spell.spellChant.match(chant) or spell.spellChant.empty()):
		spell.print_spell()							# Useful for debug purpuse.
		for t in search_targets(spell):
			spell.on_hit(t)
		chant = "" 											# Clears the player's previous input
		# TODO currentSpell.free() or similar might be necessary here.
		currentSpell = SpellBook.nix() 	# Clears the current spell
		isCasting = false

# TODO Creer une fonction commune "character_died" dans Character.gd
# TODO Comment
func character_died():
	emit_signal("player_died")

func _process(delta):
	if !isCasting and Input.is_action_just_pressed("ui_enter"):
		currentSpell = search_spell(chant)
		start_cast()
	cast_spell(currentSpell)
