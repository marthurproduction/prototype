extends "res://Character/Character.gd"

func _ready():
	SpellBook.load_spells()
	$IA/StartCasting.start()
	toggle = true

func _on_StartCasting_timeout():
	currentSpell = SpellBook.get_spell("FIREBALL")
	$IA/StartCasting.stop()
	$IA/AchiveCast.start()
	print("sart_casting")


func _on_AchiveCast_timeout():
	if chant != currentSpell.spellChant:
		currentSpell.on_hit(enemy)
	chant = ""
	$IA/StartCasting.start()
	$IA/AchiveCast.stop()
	print("spell casted")

# TODO Creer une fonction commune "character_died" dans Character.gd
# TODO comment
func character_died():
	$IA/StartCasting.stop()
	$IA/AchiveCast.stop()
	emit_signal("boss_died")
