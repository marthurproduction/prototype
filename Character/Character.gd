extends Node2D

signal player_died
signal boss_died

export (int) 		var max_life
export (String) var character_name
export (Script) var SpellBook

var life 					= max_life
var currentSpell
var enemy 				#Create a dummy enemy
var chant 				= ""
var toggle 				= false

func _ready():
	SpellBook 		= SpellBook.new()
	currentSpell 	= SpellBook.nix() #spell that does nothing
	life 					= max_life
	chant 				= ""
	toggle 				= false


# \fn Node* search_targets(spell)
# \brief Search legal target for the spell.
# \param spell: Spell.
# \return The array of legal target for the spell.
func search_targets(spell):
	match spell.targets:
		spell.ALL:
			return [self,enemy]
		spell.SELF:
			return [self]
		spell.ENEMY:
			return [enemy]
		spell.RANDOM: # TODO a bit more clean would be nice
			var arr = [self, enemy]
			return arr[randi()%2]
		_:
			return []

# \fn void take_damage(power)
# \brief player takes the power as damage
# \param power: int. the amount of damage to take
func take_damage(power):
	if (life - power) <= 0:
		life = 0
		character_died()
		print("Dead")
	elif ((life - power) >= max_life):
		life = max_life
	else:
		life -= power
		print(life)

func _unhandled_input(event):
	if not toggle:
		chant += KeyHandler.get_char(event) # Add pressed char at the end of chant
		if (event is InputEventKey and event.pressed):
			if event.scancode == KEY_BACKSPACE:
				chant = chant.substr(0,chant.length()-1) # Remove the last char TODO Better way than this
	if Input.is_action_just_pressed("ui_focus_next"):
		toggle = !toggle