extends Node

signal gui_player_init
signal gui_boss_init

# TODO Too godamn long
func _ready():
	self.connect("gui_player_init", $"GUI/SplitGUI/HUDs/Player HUD", "_on_Character_init", [$Player.character_name, $Player.max_life, $Player/Sprite.texture.load_path])
	emit_signal("gui_player_init")
	self.connect("gui_boss_init", $"GUI/SplitGUI/HUDs/Boss HUD", "_on_Character_init", [$Boss.character_name, $Boss.max_life, $Boss/Sprite.texture.load_path])
	emit_signal("gui_boss_init")
	$Player.enemy = $Boss
	$Boss.enemy = $Player
	Input.action_press("ui_cancel")
	# Initialiser le dict ici

# TODO Too godamn long
func _process(delta):
	# TODO Un dict {"NameNode": Node} pour racourcir tous les noms
	find_node("Player Chant").text = $Player.chant
	$"GUI/SplitGUI/Book/Player's Book/Text Container/Instruction".text = $Player.currentSpell.spellChant
	$"GUI/SplitGUI/Book/Boss's Book/Text Container/Boss Chant".text = $Boss.currentSpell.spellChant
	$"GUI/SplitGUI/Book/Boss's Book/Text Container/Counterspell".text = $Boss.chant
	$"GUI".runes_visibility = $Player.isCasting
	$"GUI/SplitGUI/HUDs/Boss HUD".health = $Boss.life
	$"GUI/SplitGUI/HUDs/Player HUD".health = $Player.life
	$GUI.toggle_counterspelling = $Player.toggle

func _on_GUI_restart():
	# TODO Faire une fonction pour restart le Player 	(dans Player)
	# TODO Faire une fonction pour restart le Boss 		(dans Boss)
	# TODO Faire une fonction pour restart les parties du HUD (pas obligatoire)
	$Player.life = $Player.max_life
	$Boss.life = $Boss.max_life
	
	# TODO Too godamn long
	$"GUI/SplitGUI/HUDs/Boss HUD".health = $Boss.max_life
	$"GUI/SplitGUI/HUDs/Player HUD".health = $Player.max_life
	$"GUI/SplitGUI/Book/Player's Book/Text Container/Instruction".text = ""
	$Player.currentSpell = $Player.SpellBook.nix()
	$Player.isCasting = false
	$Boss/IA/StartCasting.start()
	$Boss/IA/AchiveCast.stop()

