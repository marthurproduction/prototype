extends Node

var Spell = load("res://SpellBook/Spell/Spell.gd")
var FireBall = load("res://SpellBook/Spell/FireBall.gd")

func _ready():
	var spell = Spell.new()
	spell.print_spell()
	
	spell = FireBall.new()
	spell.print_spell()