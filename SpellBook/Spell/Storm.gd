extends "res://SpellBook/Spell/Spell.gd"

func _init():
  spellName = "STORM"
  power = 10
  targets = ALL
  modifiers = []
  spellChant = "STORM"
