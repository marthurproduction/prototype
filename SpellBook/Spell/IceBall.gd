extends "res://SpellBook/Spell/Spell.gd"

func _init():
	spellName = "ICEBALL"
	power = 2
	targets = ENEMY
	modifiers = [ ICE ]
	spellChant = "FREEZE"