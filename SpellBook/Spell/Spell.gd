extends Node

enum Target {ENEMY, SELF, ALL, RANDOM}
enum Modifier {FIRE, WATER, EARTH, AIR, LIGHT, VOID}
var spellName = "Nix"
var power = 0
var targets = ENEMY
var modifiers = []
var spellChant = ""

func _ready():
	pass

# \fn void on_hit(target)
# \brief Applies various effect on target
# \param target: ???. The target of the spell.
func on_hit(target):
	target.take_damage(power)


# \fn void print_spell()
# \brief Prints spell characteristics in the terminal
func print_spell():
	print("Spell name: " + str(spellName))
	print("Power: " + str(power))
	print("Targets: " + str(targets))
	print("Modifiers: " + str(modifiers))
	print("Chant: " + str(spellChant))
	print("\n")
