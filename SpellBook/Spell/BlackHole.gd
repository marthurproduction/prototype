extends "res://SpellBook/Spell/Spell.gd"

func _init():
  spellName = "BLACK HOLE"
  power = 50
  targets = ENEMY
  modifiers = [ VOID ]
  spellChant = "O LIGHTLESS TEMPEST FROM DISTANT REGIONS STRETCH OUT THY TENEBROUS ARMS AND LEAD MY ENEMIES TO THEIR ETERNAL SLUMBER BLACK HOLE"
