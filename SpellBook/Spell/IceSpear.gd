extends "res://SpellBook/Spell/Spell.gd"

func _init():
  spellName = "ICE SPEAR"
  power = 3
  targets = ENEMY
  modifiers = [ WATER ]
  spellChant = "FREEZE"
