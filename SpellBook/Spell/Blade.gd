extends "res://SpellBook/Spell/Spell.gd"

func _init():
  spellName = "BLADE"
  power = 5
  targets = ENEMY
  modifiers = []
  spellChant = "BLADE"
