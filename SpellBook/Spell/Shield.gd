extends "res://SpellBook/Spell/Spell.gd"

func _init():
  spellName = "SHIELD"
  power = 10
  targets = SELF
  modifiers = []
  spellChant = "SHIELD"
