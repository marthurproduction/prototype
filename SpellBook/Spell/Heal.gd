extends "res://SpellBook/Spell/Spell.gd"

func _init():
  spellName = "HEAL"
  power = -15
  targets = SELF
  modifiers = []
  spellChant = "HEAL"
