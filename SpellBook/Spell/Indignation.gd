extends "res://SpellBook/Spell/Spell.gd"

func _init():
  spellName = "INDIGNATION"
  power = 50
  targets = ENEMY
  modifiers = [ LIGHT, AIR ]
  spellChant = "I STAND IN THE PLACE BATHED IN HEAVENLY LIGHT THOU ART WHERE THE GATE TO THE UNDERWORLD OPENS COME FORTH DIVINE LIGHTNING"
