extends "res://SpellBook/Spell/Spell.gd"

func _init():
	spellName = "FIREBALL"
	power = 3
	targets = ENEMY
	modifiers = [ FIRE ]
	spellChant = "BOOM"
