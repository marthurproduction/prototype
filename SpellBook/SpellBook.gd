extends Node

var dict = {}

var Spell = load("res://SpellBook/Spell/Spell.gd")

# TODO Find a way to load all without having infinite variables
var Ball = load("res://SpellBook/Spell/Ball.gd")
var BlackHole = load("res://SpellBook/Spell/BlackHole.gd")
var Blade = load("res://SpellBook/Spell/Blade.gd")
var FireBall = load("res://SpellBook/Spell/FireBall.gd")
var Heal = load("res://SpellBook/Spell/Heal.gd")
var IceSpear = load("res://SpellBook/Spell/IceSpear.gd")
var Indignation = load("res://SpellBook/Spell/Indignation.gd")
var Judgement = load("res://SpellBook/Spell/Judgement.gd")
var Shield = load("res://SpellBook/Spell/Shield.gd")
var Storm = load("res://SpellBook/Spell/Storm.gd")

func _ready():
	# UNIT TESTS
	pass

# TODO Find a way to load all without having infinite variables
# \fn void load_spells()
# \brief loads all the spell known to the dict
func load_spells():
	add_spell(Ball.new())
	add_spell(BlackHole.new())
	add_spell(Blade.new())
	add_spell(FireBall.new())
	add_spell(Heal.new())
	add_spell(IceSpear.new())
	add_spell(Indignation.new())
	add_spell(Judgement.new())
	add_spell(Shield.new())
	add_spell(Storm.new())

# \fn void add_spell(spell)
# \brief Adds the spell to the dictionary
# \param spell: Spell. The spell to add.
# TODO Might not be the cleanest way But works.
func add_spell(spell):
	var key = spell.spellName
	if dict.has(key):
		dict.erase(key)
	dict[key] = spell

# \fn Spell nix()
# \brief Return the "empty" spell
# \returns Spell. A spell that does nothing.
func nix():
	return Spell.new()

# \fn Spell get_spell(spellName)
# \brief Fetch the Spell "spellName" in the SpellBook.
# \param spellName: String. The name of the spell.
# \return Spell. The Spell with the name spellName
func get_spell(spellName):
	if dict.has(spellName):
		return dict[spellName]
	else:
		#return nix()
		return parse(spellName)

# \fn Spell parse(spellName)
# \brief Creates a new spell
# \param spellName: String. The custom spell.
# \return Spell. A spell with the chosen characteritcs.
func parse(spellName):
	var arr = spellName.split(" ")
	var spell = nix()
	spell.spellName = spellName
	for i in arr:
		match i:
			"FIRE":
				spell.modifiers.append(Spell.FIRE)
			"WATER", "ICE":
				spell.modifiers.append(Spell.WATER)
			"EARTH":
				spell.modifiers.append(Spell.EARTH)
			"AIR":
				spell.modifiers.append(Spell.AIR)
			"LIGHT", "HOLY":
				spell.modifiers.append(Spell.LIGHT)
			"VOID", "DARK", "DARKNESS":
				spell.modifiers.append(Spell.VOID)
			_:
				return nix()
	return spell