extends Node

var keyEcho

func _ready():
	keyEcho = []

# \fn String get_char(event)
# \brief Return the keyboard char inputs.
# \details In upper case. Without modifiers (Alt, Shift, ...)
# Should be called inside the "_unhandled_input" function as follow:
# func _unhandled_input(event):
#	var c = get_char(event)
# \param event: the event to analyse
# \return String. Char pressed on the keybord. Empty char if no press.
func get_char(event):
	var arrInput = []
	if (event is InputEventKey and event.pressed):
		if (event.scancode <= KEY_Z and event.scancode >= KEY_A):
			arrInput = event.as_text().split('+') # Remove the modifiers (Shift,...)
			return arrInput[-1]
		if event.scancode == KEY_SPACE:
			return " "
	return ""