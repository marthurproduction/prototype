extends ColorRect


func _ready(): #set the color to black with full transparencies
	color = Color(0, 0, 0, 0)

func _on_Pause_Menu_alpha_changed(value): #set the transparency to value/255
	color = Color(0, 0, 0, value / 255.0)
