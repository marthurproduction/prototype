extends "res://GUI/HUDs.gd"

var animated_health = 0
var health

func _ready():
	pass

# TODO Passer le process a HUDs.gd
func _process(delta):
	$Tween.interpolate_property(self, "animated_health", animated_health, health, 0.3, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$"Info Container/Info Background/PV Print".text = str(round(animated_health))
	$"Info Container/Info Background/PV Bar".value = animated_health
	if not $Tween.is_active():
		$Tween.start()