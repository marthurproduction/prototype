extends Container


var is_in_pause
var runes_visibility
var toggle_counterspelling

signal restart

# TODO Too godamn long
# Regroup variable by type
# Hide using the Node Tree? (optional)
func _ready():
	var screensize = get_viewport_rect().size
	is_in_pause = false
	# Create a function for Vector2(...)
	$"SplitGUI/HUDs/Player HUD/Portrait Container".rect_min_size = Vector2(screensize.x / 6, screensize.y / 3)
	$"SplitGUI/HUDs/Player HUD/Info Container".rect_min_size = Vector2(screensize.x / 3, screensize.y / 3)
	$"SplitGUI/HUDs/Boss HUD/Portrait Container".rect_min_size = Vector2(screensize.x / 6, screensize.y / 3)
	$"SplitGUI/HUDs/Boss HUD/Info Container".rect_min_size = Vector2(screensize.x / 3, screensize.y / 3)
	$"SplitGUI/Book/Player's Book".rect_min_size = Vector2(screensize.x / 2, screensize.y * 2 / 3)
	$"SplitGUI/Book/Boss's Book".rect_min_size = Vector2(screensize.x / 2, screensize.y * 2 / 3)
	$"SplitGUI/Book/Boss's Book/Text Container/Runes2".hide()
	$"Pause Menu".hide()
	$"Pause Menu".set_disable_button(true)
	runes_visibility = false
	$RestartButton.disabled = true

# Regroup by type (show() ensemble, ...)
# \fn void resume()
# \brief hide endScreen and pause menu, show the main scene and disable button
func resume():
	$DeathScreen.hide()
	$WinScreen.hide()
	is_in_pause = false
	$"Pause Menu".hide()
	$SplitGUI.show()
	$"Pause Menu".set_disable_button(true)

func _on_Pause_Menu_restart():
	get_tree().paused = false
	emit_signal("restart")
	resume()

func _on_Pause_Menu_resume():
	get_tree().paused = false
	resume()

# TODO Too godamn long
func _process(delta):
	# TODO Create a pause function
	if Input.is_action_just_pressed("ui_cancel") and !is_in_pause:
		is_in_pause = true
		$"Pause Menu".show()
		$"Pause Menu/Main Pause Menu".show()
		$SplitGUI.hide()
		$"Pause Menu/Option Menu".hide()
		$"Pause Menu".set_disable_button(false)
		get_tree().paused = true

	# TODO Fonctionne pas
	elif Input.is_action_just_pressed("ui_cancel") and is_in_pause:
		resume()

	# TODO Create a function rune show 
	# maybe not each frame? (optional)
	if toggle_counterspelling:
		$"SplitGUI/Book/Player's Book/Text Container/Runes3".hide()
	else:
		$"SplitGUI/Book/Player's Book/Text Container/Runes3".show()
		if runes_visibility:
			$"SplitGUI/Book/Player's Book/Text Container/Runes".show()
		else:
			$"SplitGUI/Book/Player's Book/Text Container/Runes".hide()
	if toggle_counterspelling:
		$"SplitGUI/Book/Boss's Book/Text Container/Runes2".show()
	else:
		$"SplitGUI/Book/Boss's Book/Text Container/Runes2".hide()

func _on_Player_player_died():
	$SplitGUI.hide()
	$DeathScreen.show()
	$RestartButton.show()
	$RestartButton.disabled = false


func _on_Boss_boss_died():
	$SplitGUI.hide()
	$WinScreen.show()
	$RestartButton.show()
	$RestartButton.disabled = false

# TODO Rename plz
func _on_Button_pressed():
	$DeathScreen.hide()
	$WinScreen.hide()
	$RestartButton.hide()
	$RestartButton.disabled = true
	get_tree().paused = false
	emit_signal("restart")
	resume()
