extends HBoxContainer

func _ready():
	pass

# TODO Too godamn long (optional)
func _on_Character_init(character_name, life, portrait_load_path):
	$"Portrait Container/Portrait".texture.load_path = portrait_load_path
	$"Info Container/Info Background/Name".text = character_name
	$"Info Container/Info Background/PV Print".text = str(life)
	$"Info Container/Info Background/PV Bar".value = life
	$"Info Container/Info Background/PV Bar".max_value = life
