extends MarginContainer

signal resume
signal restart
signal option
signal alpha_changed


# \fn void set_disable_button(b)
# \brief Sets every Button in "Buttons Container" to b.
# \param b: bool. The value Button.disabled takes
func set_disable_button(b):
	for i in $"Main Pause Menu/Buttons Container".get_children():
		i.disabled = b

func _on_Restart_Buttons_pressed():
	emit_signal("restart")


func _on_Exit_Button_pressed():
	get_tree().quit()


func _on_Options_Button_pressed():
	set_disable_button(true)
	$"Main Pause Menu".hide()
	$"Option Menu".show()


func _on_Resume_Button_pressed():
	emit_signal("resume")


func _on_HScrollBar2_value_changed(value):
	emit_signal("alpha_changed", value)


func _on_Back_pressed():
	$"Option Menu".hide()
	$"Main Pause Menu".show()
	set_disable_button(false)
